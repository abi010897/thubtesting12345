FROM node:10.16.0-alpine

RUN mkdir -p /app && mkdir -p /run/nginx/ && \
        mkdir -p /var/www/html

WORKDIR /app

RUN apk update && apk add --no-cache nginx && \
    rm -rf /var/cache/apk/* && \
    chown -R nginx:nginx /var/www/html && \
    chown -R nginx:nginx /run/nginx

COPY . /app

COPY nginx.conf /etc/nginx/conf.d/default.conf
